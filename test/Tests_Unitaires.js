var form_OK = true;
var champ_Prenom = "Jonathan";
var champ_Nom = "Virolan";
var champ_Mail = "jonathan.virolan@ensg.eu";
var champ_Adresse = "3 place du bois de la grange, Noisiel";
var champ_Numero = "0689491276";

function Test_Prenom(form_OK, champ_Prenom){
    if(champ_Prenom == ""){
            form_OK = false;
    }
    else if(!isNaN(parseInt(champ_Prenom))){
        form_OK = false;
    }
    return form_OK;
}

function Test_Nom(form_OK, champ_Nom){
    if(champ_Nom != ""){
        if(!isNaN(parseInt(champ_Nom))){
            form_OK = false;
        }
    }

    return form_OK;
}

/**
*@param {string} str dsqdzed
*
*/

function Test_Mail(form_OK, champ_Mail){
    var regex = /^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]­{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/
    if (champ_Mail == "") {
        form_OK = false;
    }
    else if (regex.exec(champ_Mail.toLowerCase()) == null) {
        form_OK = false;
    }

    return form_OK;
}

function Test_Adresse(form_OK, champ_Adresse){
    if(champ_Adresse == ""){
        form_OK = false;
    }

    return form_OK;
}

function Test_Numero(form_OK, champ_Numero){
    if(champ_Numero != ""){ // Le champ Numéro n'est pas obligatoire donc si on décide de remplir ce champ alors ...
        if(champ_Numero.length != 10){ // ... on vérifie si c'est bien un numéro de téléphone (sa taille est équivaut à 10).
            form_OK = false;
        }
    }

    return form_OK;
}

function Test_Formulaire(champ_Prenom, champ_Nom, champ_Mail, champ_Adresse, champ_Numero){

    form_OK = true
    form_OK = Test_Prenom(form_OK,champ_Prenom);
    form_OK = Test_Nom(form_OK, champ_Nom);
    form_OK = Test_Mail(form_OK, champ_Mail);
    form_OK = Test_Adresse(form_OK, champ_Adresse);
    form_OK = Test_Numero(form_OK, champ_Numero);

    return form_OK;

}

QUnit.test("Test Prenom", function( assert ) {
  assert.equal(Test_Prenom(form_OK,champ_Prenom), true, "Prenom correcte" );
});

QUnit.test("Test Nom", function( assert ) {
  assert.equal(Test_Nom(form_OK,champ_Nom), true, "Même chose" );
});

QUnit.test("Test Mail", function( assert ) {
  assert.equal(Test_Mail(form_OK,champ_Mail), true, "Même chose" );
});

QUnit.test("Test Adresse", function( assert ) {
  assert.equal(Test_Adresse(form_OK,champ_Adresse), true, "Même chose" );
});

QUnit.test("Test Numero", function( assert ) {
  assert.equal(Test_Numero(form_OK,champ_Numero), true, "Même chose" );
});

QUnit.test("Test Formulaire", function( assert ) {
  assert.equal(Test_Formulaire(champ_Prenom, champ_Nom, champ_Mail, champ_Adresse, champ_Numero), true, "Même chose" );
});
