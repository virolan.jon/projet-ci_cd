# Projet CI_CD - VIROLAN Jonathan
Le but de ce projet d'intégration/déploiement continue était de créer une pipeline réalisant 3 étapes différentes (build, test & deploy). Ceci devait être réaliser sur un projet (de n'importe quel type) soit obtenu par nous même soit proposé par notre professeur.
Dans mon cas, j'ai décidé de choisir un projet JavaScript.

### *Tâches réalisées :*

 - Stage Build : 100%
	 - Construction d'un site web statique de documentation via l'API de Jsdoc.
 - Stage Test : 50%
	 - Des tests sont effectué à l'aide de l'API PhontomJS et du framework QUnit.
 - Stage Deploy : 100 %
	 - Deploiement de mon site web statique à cette  [adresse](https://virolan.jon.gitlab.io/projet-ci_cd/main/out/global.html).
	 
### *Explication du Pipeline :*

    image: node:latest
Lance un docker possedant la dernière version de NodeJS dans l'invite de commande.


    stages:
     - build
     - test
     - deploy
Contient les 3 stages (build, test & deploy) qui vont être effectuer lors du lancement de la pipeline.


    default:
    	before_script:
    	     - export OPENSSL_CONF=$WORKSPACE/openssl.cnf
Lors de chaque script lancé, peu importe le stage, cette ligne de commande est effectuer. Elle permet de lancer correctement mes fichiers shell.

    build:
		stage: build
		script:
	  	    - echo "Lancement du Build"
	  	    - npm install -g jsdoc
	  	    - ./scripts_sh/build.sh
Ce job est effectué lors du stage build (au tout début). Il possède 3 différents script, en plus du script explique précédemment : un affichage disant que le build se lance, l'installation de l'API Jsoc, permettant la construction du site web statique de documentation et l’exécution du fichier 'build. sh' qui créer le site.

    test:
        stage: test
        script:
        	- echo "Lancement des Tests"
        	-  ./scripts_sh/test.sh
Celui-ci se lance pendant le stage test (après le stage build). Durant ce stage, un fichier shell est exécuté permettant de lancer les tests. Si un des test est faux alors la pipeline s'arrete et la Merge Request ne s'effectue pas sinon elle passe au stage suivant.

  

    pages:
	   	stage: deploy
	    script:
	  	    - mkdir .public
	  	    - cp -r * .public
	  	    - mv .public public
	      artifacts:
	      paths:
	  	    - public
	      only:
	  	    - master

Pour finir le job pages, se trouvant dans le stage deploy (le dernier). il permet de placer le dossier du projet dans un dossier public, uniquement sur la branche master, les pages déployer peuvent être ouvertes via le service Gitlab Pages. 
 
### *Utilisation :*
Pour la récupération du code, il vous suffit de vous placer dans le dossier de votre choix et d'y ouvrir une invite de commande et de lancer cette commande : 

    $ git clone git@gitlab.com:virolan.jon/projet-ci_cd.git
    
Pour changer les tests dirigez vous dans le dossier test et ouvrez le fichier Tests_Unitaires.js et modifiez l'une des variables commençant par 'champ_' par example vous pouvez mettre des numéro dans le champ_prenom pour que le test soit faux.

La **visualisation** de l'avancement de la pipeline, des différents stages, se fait dans le sous-menu CI/CD => Pipelines.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc0NDQ0MTEwNywxODczODU3Mjg3LC0xMz
I5NDc3MzY5LC0xMDkwMzgwNTAzXX0=
-->